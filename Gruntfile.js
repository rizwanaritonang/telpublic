/* ------------------------------------------------------------------------- *
 * GLOBALS MODULE, REQUIRE
/* ------------------------------------------------------------------------- */

module.exports = function(grunt) {

	"use strict";

  
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),


    /* UGLIFY CONFIGURATION
    /* ------------------------------------ */
	uglify: {
		global: {
			files: {
				"js/script.min.js": ["js/scripts.js"]
			}
		}
	},

	/* AUTOPREFIXER CONFIGURATION
	/* ------------------------------------ */
	autoprefixer:{
		global: {
			src: "css/app-unprefixed.css",
			dest: "css/app.css"
		}
	}, 

	/* SASS CONFIGURATION
	/* ------------------------------------ */
    sass: {
    	global: {
    		options: {
    			style: "expanded"
    		},
    		files: {
    			"css/app-unprefixed.css": "sass/application.scss"
    		}
    	}
    },

    /* JSHINT CONFIGURATION
    /* ------------------------------------ */
    jshint: {
    	options: {
    		force: true
    	},
    	all: ['Gruntfile.js', 'js/scripts.js'],
    },


    /* HTMLHINT CONFIGURATION
    /* ------------------------------------ */
    htmlhint: {
    	build:{
    		options:{
	            'tag-pair': true,
	            'tagname-lowercase': true,
	            'attr-lowercase': true,
	            'attr-value-double-quotes': true,
	            'doctype-first': true,
	            'spec-char-escape': true,
	            'id-unique': true,
	            'head-script-disabled': true,
	            'style-disabled': true
    		}, // options
    		src: ['*.html']
    	}
    },


    watch: {
		options: {
			livereload: true,
		},

    	gruntfile: {
    		files: 'Gruntfile.js',
    		tasks: ['jshint:gruntfile'],
    	},

    	scripts: {
    		files: ['js/scripts.js'],
    		tasks: ['jshint', 'uglify'],
    	},

    	css: {
    		files: '**/*.scss',
    		tasks: ['sass', 'autoprefixer'],
    	}, // css

        html: {
            files: ['*.html'],
            tasks: ['htmlhint:build'],
        }, // html

    } // watch
  });

  require("load-grunt-tasks")(grunt);

  // REGISTER GRUNT TASKS
  grunt.registerTask( 'default', [ 'uglify', 'jshint', 'sass', 'autoprefixer', 'htmlhint', 'newer:assemble', 'prettify' ] );
  grunt.registerTask( 'serve', [ 'sass', 'autoprefixer', 'watch'] );

};